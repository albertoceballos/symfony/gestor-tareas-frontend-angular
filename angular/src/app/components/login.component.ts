import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';


@Component({
	selector: 'login',
	templateUrl:'../views/login.html',
	styleUrls: ['../app.component.css'],
	providers:[UserService]
})

export class LoginComponent implements OnInit{

	public title;
	public user;
	public identity;
	public token;

	constructor(private _route:ActivatedRoute,private _router: Router,private _userService:UserService){
		this.title="Identifícate";
		this.user={
			"email":"",
			"password":"",
			"getHash":true,
		};
	}

	ngOnInit(){

		console.log('El componente login ha sido cargado correctamente');
		this.logout();
		this.redirectIfLogged();

	}

	onSubmit(){
		console.log(this.user);
		this._userService.signUp(this.user).subscribe(
				response=>{
					this.identity=response;
					if(this.identity<=1){
						console.log("Error en  el servidor");
					}else{
						if(!this.identity.status){
							//console.log(this.identity);
							localStorage.setItem('identity', JSON.stringify(this.identity));
							console.log(JSON.parse(localStorage.getItem('identity')));
							this.user.getHash=false;
							this._userService.signUp(this.user).subscribe(
											response=>{
												this.token=response;
												if(this.token<=1){
													console.log("Error en  el servidor");
												}else{
													if(!this.token.status){
														//console.log(this.identity);
														localStorage.setItem('token', JSON.stringify(this.token));
														console.log(JSON.parse(localStorage.getItem('token')));		
														window.location.href="/";	
													}
												}

											},
											error=>{
												console.log("Error")
							});

						}
					}

				},
				error=>{
					console.log("Error")
		});
		
	}

	logout(){

		this._route.params.forEach((params)=>{
			let logout= parseInt(params['id']);
			if(logout==1){
				localStorage.removeItem('identity');
				localStorage.removeItem('token');
				this.identity=null;
				this.token=null;
				window.location.href="/login";
				console.log("Metodo logout llamado");
			}

		});
	}

	redirectIfLogged(){
		let identity=this._userService.getIdentity();
		if( identity!=null && identity.sub){
			this._router.navigate(['/']);
		}
	}

}


