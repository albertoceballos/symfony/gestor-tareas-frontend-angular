import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/User';
import { UserService } from '../services/user.service';

@Component({
	selector:'user-edit',
	templateUrl:'../views/user.edit.html',
	styleUrls: ['../app.component.css'],
	providers: [UserService],
})

export class UserEditComponent implements OnInit{

	public title;
	public user:User;
	public identity;
	public token;
	public password_visible;
	public response;
	public status;


	constructor(private _router: Router, private _ActivatedRoute: ActivatedRoute, private _userService: UserService){
		this.title="Modificar mis datos";
		this.identity=this._userService.getIdentity();
		this.token=this._userService.getToken();
		this.password_visible=false;

	}

	ngOnInit(){
		if(this.identity==null){
			this._router.navigate(['login']);
		}else{
			this.user=new User(
				this.identity.id,
				this.identity.role,
				this.identity.name,
				this.identity.surname,
				this.identity.email,
				this.identity.password		
				);

		}

	}

	viewPassword(){
		if(this.password_visible==false)
			this.password_visible=true;
		else{
			this.password_visible=false;
		}
	}

	onSubmit(){
		console.log(this.user);

		this._userService.updateUser(this.user).subscribe(
				response=>{
					this.response=response;
					if(this.response.status!='success'){
						this.status="error";
					}else{
						this.status="success";
						localStorage.setItem('identity', JSON.stringify(this.user));
					}
				},
				error=>{
					console.log(<any>error);
				}
			);
	}


}