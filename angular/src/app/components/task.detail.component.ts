import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
import { Task } from '../models/Task';

@Component({
  selector: 'task-detail',
  templateUrl: '../views/task.detail.html',
  styleUrls: ['../app.component.css'],
  providers: [UserService,TaskService],
})
export class TaskDetailComponent implements OnInit {
  public identity;
  public token;
  public response;
  public status;
  public task:Task;
  public loading;

  constructor(private _router:Router, private _activatedRoute:ActivatedRoute, private _userService:UserService, private _taskService:TaskService)
  {
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.loading=true;
  }

  ngOnInit() {
    if(this.identity && this.identity.sub){
      this.getTask();
    }else{
      this._router.navigate(['/login']);
    }
  }

  getTask(){
    this._activatedRoute.params.forEach((params:Params)=>{
      let id= parseInt(params['id']);

      this._taskService.getTask(id,this.token).subscribe(
        response=>{
          this.response=response;
          if(this.response.status!='success'){
            this.status='error';
            console.log("Error!");

          }else{
            if(this.response.data.user.id==this.identity.sub){
              this.status="success";
              this.loading=false;
              this.task=this.response.data;
              console.log(this.task);
            }else{
              this._router.navigate(['/']);
            }
          }
        },
        error=>{
          console.log(<any>error);
        }
      );

    });
  }

  removeTask(id){
    console.log("Has pulsado borrar");
    this._taskService.remove(id,this.token).subscribe(
      response=>{
        this.response=response;
        if(this.response.status!="success"){
          alert("No se ha podido borrar la tarea");
        }else{
          this._router.navigate(['index']);
        }
      },
      error=>{
        console.log(<any>error);
      }
    );

  }

}
