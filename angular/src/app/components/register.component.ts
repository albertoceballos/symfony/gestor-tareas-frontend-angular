import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/User';
import { UserService } from '../services/user.service';


@Component({
	selector: 'register',
	templateUrl:'../views/register.html',
	styleUrls: ['../app.component.css'],
	providers:[UserService],
})

export class RegisterComponent implements OnInit{

	public title="Registro";
	public user:User;
	public response;
	public status;

	constructor(private _route:ActivatedRoute, private _router:Router,private _userService:UserService){
		this.user=new User(1,"user","","","","");
	}

	ngOnInit(){

		console.log("El componente register ha sido cargado correctamente");
	}

	onSubmit(){
		console.log(this.user);
		this._userService.register(this.user).subscribe(
			response=>{
				this.response=response;		
				console.log(response);
				if(this.response.status!="success"){
					this.status="error";
				}else{
					this.status="success";
					this.user=new User(1,"user","","","","");
				}
			},
			error=>{
				console.log(error);
			},
		);
	}


}