import { Component,OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Task } from '../models/Task';
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
import * as $ from 'jquery';



@Component({
	selector: 'default',
	templateUrl:'../views/default.html',
	styleUrls: ['../app.component.css'],
	providers:[TaskService, UserService],
})

export class DefaultComponent implements OnInit{

	public title="Home page";
	public identity;
	public token;
	public tasks:Array<Task>;
	public response;
	public status;
	public pages;
	public pagePrev;
	public pageNext;
	public loading;
	//propiedades para el formulario de búsqueda:
	public filter;
	public order;
	public searchString;

	

	constructor(
		private _activatedRoute:ActivatedRoute,
		private _router:Router,
		private _userService:UserService,
		private _taskService:TaskService
	)
	{
		this.token=this._userService.getToken();
		this.identity=this._userService.getIdentity();
		this.loading=true;
		this.filter=0;
		this.order=0;

	}

	ngOnInit(){
	
		console.log("El componente default ha sido cargado correctamente");
		this.getAllTasks();
	}

	getAllTasks(){
		//conseguir parámetros de get
		this._activatedRoute.params.forEach((params:Params)=>{
			let page= parseInt(params['page']);
			if(!page){
				page=1;
			}

			this._taskService.getTasks(this.token,page).subscribe(
				response=>{
					this.loading=false;
					this.response=response;
					if(this.response.status!='success'){
						this.status='error';
					}else{
						this.status='success';
						this.tasks=this.response.data;
						console.log(this.tasks);

						//meto las páginas del total de páginas de la paginación
						this.pages=[];
						for(let i=1;i <= this.response.total_pages;i++){
							this.pages.push(i);
						}

						//página anterior
						if(page >= 2){
							this.pagePrev=page-1;
						}else{
							this.pagePrev=page;
						}
						//página siguiente
						if(page < this.response.total_pages){
							this.pageNext=page+1;
						}else{
							this.pageNext=page;
						}

					}
				},
				error=>{
					console.log(<any> error);
				}
			);
			
		});

	}

	search(){

		if(!this.searchString || this.searchString.trim().length==0){
			this.searchString=null;
		}

		this._taskService.search(this.token, this.searchString, this.filter, this.order).subscribe(
			response=>{
				this.response=response;
				if(this.response.status!='success'){
					this.tasks=null;
				}else{
					this.tasks=this.response.data;
					console.log(this.tasks);
				}
				
			},
			error=>{
				console.log(<any>error);
			}
		);


	}

	

}