import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
//servicios:
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
//modelo:
import { Task } from '../models/Task';


@Component({
	selector: 'task-edit',
	templateUrl: '../views/task.new.html',
	styleUrls: ['../app.component.css'],
	providers: [UserService, TaskService],

})

export class TaskEditComponent implements OnInit {
	public titulo: string;
	public identity;
	public task: Task;
	public token;
	public response;
	public status_task;
	public loading;
	public titulo_boton

	constructor(private _router: Router, private _ActivatedRoute: ActivatedRoute, private _userService: UserService, private _taskService: TaskService) {
		this.titulo = "Modificar tarea";
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.titulo_boton="Actualizar";
		this.loading=true;
	}

	ngOnInit() {
		if (this.identity == null && !this.identity.sub) {
			this._router.navigate(['login']);
		} else {
			this.getTask();
		}
	}

	onSubmit() {
		console.log(this.task);
		this._ActivatedRoute.params.forEach((params: Params) => {
			let id = parseInt(params['id']);
			this._taskService.update(this.token, this.task, id).subscribe(
				response => {
					this.response = response;
					if (this.response.status != 'success') {
						this.status_task = "error";
					} else {
						this.status_task = "success";
						this.task = this.response.data;
						this._router.navigate(['/']);

					}
				},
				error => {
					console.log(<any>error);
				}
			);
		});

	}

	getTask() {
		this._ActivatedRoute.params.forEach((params: Params) => {
			let id = parseInt(params['id']);

			this._taskService.getTask(id, this.token).subscribe(
				response => {
					this.response = response;
					if (this.response.status != 'success') {
						this.status_task = 'error';
						console.log("Error!");

					} else {
						if (this.response.data.user.id == this.identity.sub) {
							this.loading = false;
							this.task = this.response.data;
							console.log(this.task);
						} else {
							this._router.navigate(['/']);
						}
					}
				},
				error => {
					console.log(<any>error);
				}
			);

		});
	}


}