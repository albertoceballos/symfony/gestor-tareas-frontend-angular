import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
//servicios:
import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';
//modelo:
import { Task } from '../models/Task';


@Component({
	selector:'task-new',
	templateUrl:'../views/task.new.html',
	styleUrls: ['../app.component.css'],
	providers: [UserService,TaskService],

})

export class TaskNewComponent implements OnInit{
	public titulo:string;
	public identity;
	public task:Task;
	public token;
	public response;
	public status_task;
	public titulo_boton;

	constructor(private _router:Router, private _ActivatedRoute:ActivatedRoute, private _userService:UserService, private _taskService:TaskService){
		this.titulo="Crear nueva tarea";
		this.identity=this._userService.getIdentity();
		this.token=this._userService.getToken();
		this.titulo_boton="Crear";
	}

	ngOnInit(){
		if(this.identity==null && !this.identity.sub){
			this._router.navigate(['login']);
		}else{
			this.task=new Task(1, '','','nueva',null,null);
		}
	}

	onSubmit(){
		console.log(this.task);
		this._taskService.create(this.token,this.task).subscribe(
				response=>{
					this.response=response;
					if(this.response.status!='success'){
						this.status_task="error";
					}else{
						this.status_task="success";
						this.task=this.response.data;
						this._router.navigate(['/']);

					}
				},
				error=>{
					console.log(<any>error);
				}
			);

	}


}