export class User{

	public id:number;
	public role:string;
	public name:string;
	public surname:string;
	public email:string;
	public password:string;

	constructor(id,role,name,surname,email,password){
		this.id=id;
		this.role=role;
		this.name=name;
		this.surname=surname;
		this.email=email;
		this.password=password;
	}

}