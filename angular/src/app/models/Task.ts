export class Task{
	public id:number;
	public title:string;
	public description:string;
	public status:string;
	public createdAt;
	public updatedAt;

	constructor(id,title,description,status,createdAt,updatedAt){
		this.id=id;
		this.title=title;
		this.description=description;
		this.status=status;
		this.createdAt=createdAt;
		this.updatedAt=updatedAt;

	}


}