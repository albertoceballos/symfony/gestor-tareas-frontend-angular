import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { GLOBAL } from './global';


@Injectable(

)
export class UserService {

  public url: string;
  public identity;
  public token;

  constructor(private httpClient: HttpClient) {
    this.url = GLOBAL.url;
  }

  signUp(user_to_login) {
    let json = JSON.stringify(user_to_login);
    let params = "json=" + json;
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    return this.httpClient.post(this.url + 'login', params, { headers: headers });
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem('identity'));

    if (identity != 'undefined') {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    return this.identity;

  }

  getToken() {
    let token = JSON.parse(localStorage.getItem('token'));

    if (token != 'undefined') {
      this.token = token;
    } else {
      this.token = null;
    }
    return this.token;
  }

  register(user_to_register) {
    let json = JSON.stringify(user_to_register);
    let params = "json=" + json;
    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.httpClient.post(this.url + 'user/new', params, { headers: headers });
  }

  updateUser(user_to_update) {
    let json = JSON.stringify(user_to_update);
    let params = "json=" + json + '&authorization=' + this.getToken();
    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.httpClient.post(this.url + 'user/edit', params, { headers: headers });

  }

}
