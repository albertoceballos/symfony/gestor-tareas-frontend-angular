import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { GLOBAL } from './global';


@Injectable(

)
export class TaskService {
	public url: string;

	constructor(private _httpClient: HttpClient) {
		this.url = GLOBAL.url;

	}

	create(token, task) {
		let json = JSON.stringify(task);
		let params = 'json=' + json + '&authorization=' + token;
		let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

		return this._httpClient.post(this.url + 'task/new', params, { headers: headers });
	}

	getTasks(token, page = null) {
		let params = "authorization=" + token;
		let headers = new HttpHeaders({ 'Content-type': 'application/x-www-form-urlencoded' });

		return this._httpClient.post(this.url + 'task/list?page=' + page, params, { headers: headers });

	}

	getTask(idTask, token) {
		let params = "authorization=" + token;
		let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

		return this._httpClient.post(this.url + 'task/detail/' + idTask, params, { headers: headers });
	}

	update(token, task, idTask) {

		let json = JSON.stringify(task);
		let params = "json=" + json + "&authorization=" + token;
		let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

		return this._httpClient.post(this.url + 'task/new/' + idTask, params, { headers: headers });
	}

	search(token,search=null,filter=null,order=null){
		let params="authorization="+token+"&order="+order+"&filter="+filter;
		let headers=new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
		let url:string;
		if (search==null){
			url=this.url+'task/search';
		}else{
			url=this.url+'task/search/'+search;
		}

		return this._httpClient.post(url, params, {headers:headers});

	}

	remove(idTask,token){
		let params="authorization="+token;
		let headers=new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
		
		return this._httpClient.post(this.url+'task/remove/'+idTask, params, {headers:headers});

	}



}
