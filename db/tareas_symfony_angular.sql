-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-02-2019 a las 12:24:07
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tareas_symfony_angular`
--
CREATE DATABASE IF NOT EXISTS `tareas_symfony_angular` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `tareas_symfony_angular`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `description` text COLLATE utf8_spanish_ci,
  `status` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tasks`
--

INSERT INTO `tasks` (`id`, `user_id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 9, 'tarea1', 'una tarea de luis', 'nueva', '2019-02-15 10:27:58', '2019-02-28 11:18:41'),
(2, 9, 'nueva tarea luis', 'otra tarea de luis 2', 'nueva', '2019-02-15 10:58:08', '2019-02-15 11:29:29'),
(3, 9, 'tarea 3', 'descripción tarea 3', 'acabada', '2019-02-15 11:56:36', '2019-02-15 11:56:36'),
(4, 9, 'tarea 4', 'tarea numero 4', 'nueva', '2019-02-16 00:00:00', '2019-02-16 00:00:00'),
(5, 8, 'tarea prueba 6', 'descripción tarea de prueba 6', 'nueva', '2019-02-18 00:00:00', '2019-02-18 00:00:00'),
(6, 8, 'nueva tarea', 'descripcion yo que se', 'pendiente', '2019-02-25 13:56:19', '2019-02-25 13:56:19'),
(7, 8, 'crear algo en Angular', 'algo bonito con Angular', 'nueva', '2019-02-25 14:11:40', '2019-02-25 14:11:40'),
(8, 9, 'programar el front-end ', 'front-end con Angular 7', 'acabada', '2019-02-26 00:00:00', '2019-02-27 13:04:30'),
(12, 9, 'tarea 10', 'descripción tarea 10', 'acabada', '2019-02-25 00:00:00', '2019-02-25 00:00:00'),
(13, 9, 'tarea 11', 'descripción tarea 11', 'acabada', '2019-02-22 00:00:00', '2019-02-23 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `surname`, `email`, `password`, `created_at`) VALUES
(1, 'ROLE_USER', 'admin', 'admin', 'admin@admin.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-02-12 00:00:00'),
(8, 'ROLE_USER', 'Maria', 'García López', 'maria@maria.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-02-14 20:35:24'),
(9, 'ROLE_USER', 'Luis', 'Díaz', 'luis@luis.com', 'c5ff177a86e82441f93e3772da700d5f6838157fa1bfdc0bb689d7f7e55e7aba', '2019-02-14 20:38:33'),
(10, 'ROLE_USER', 'dfdfdfdf', 'Garcia', 'aaaa@dddd.es', 'd17f25ecfbcc7857f7bebea469308be0b2580943e96d13a3ad98a13675c4bfc2', '2019-02-22 14:36:11'),
(11, 'ROLE_USER', 'teo', 'dfdf', 'teo@teo.com', '0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c', '2019-02-22 14:46:31'),
(12, 'ROLE_USER', 'Rubén', 'Sánchez', 'ruben@ruben.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-02-23 10:30:55'),
(13, 'ROLE_USER', 'Miguel', 'Fernández', 'miguel@fernandez.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-02-23 10:37:33'),
(14, 'ROLE_USER', 'Elena', 'Menéndez', 'elena@elena.es', '0ce93c9606f0685bf60e051265891d256381f639d05c0aec67c84eec49d33cc1', '2019-02-23 10:49:24');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
