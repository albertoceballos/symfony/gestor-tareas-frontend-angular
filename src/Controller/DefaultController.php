<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;
//servicios 
use App\Service\NumeroAleatorio;
use App\Service\Helpers;
use App\Service\JwtAuth;
//entidades
use App\Entity\Users;

class DefaultController extends AbstractController {


    public function login(Request $request, Helpers $helpers, JwtAuth $jwt_auth) {

        //Recibir Json por POST:
        $json = $request->get('json', null);

        //array a devolver por defecto:
        $data = [
            'status' => 'Error',
            'data' => 'Manda Json por via POST',
        ];


        //hacer login si no viene el post vacío:
        if ($json != null) {

            //decodifico el objeto json de la varibale $json en un onjeto PHP:
            $params = json_decode($json);

            if (isset($params->email)) {
                $email = $params->email;
            } else {
                $email = null;
            }
            if (isset($params->password)) {        
                $password = $params->password;
               
            } else {
                $password = null;
            }
            if (isset($params->getHash)) {
                $getHash = $params->getHash;
            } else {
                $getHash = null;
            }



            $validator = Validation::createValidator();
            $validate_email = $validator->validate($email, [new Email()]);
            $pwd= hash('sha256', $password);
            
            if ($email != null && count($validate_email) == 0 && $password != null) {     
                if ($getHash == null || $getHash == false) {
                    $signup = $jwt_auth->signup($email, $pwd);
                } else {
                    $signup = $jwt_auth->signup($email, $pwd, true);
                }

                return $this->json($signup);

            }else{
                $data = [
                    'satatus' => 'error',
                    'data' => 'Email o Password incorrecto',
                ];
            }
        }

        return $helpers->jsonConverter($data);
    }
    
    
    public function pruebas(Request $request, Helpers $helpers, JwtAuth $JwtAuth){
        
        $token=$request->get('authorization',null);
        
        if($token && $JwtAuth->checkToken($token)==true){         
            $em= $this->getDoctrine()->getManager();
            $user_repo=$em->getRepository(Users::class);
            $users=$user_repo->findAll();
            
            $data=$helpers->jsonConverter([
               'status'=>'ok',
                'users'=>$users,
            ]);         
            
        }else{
            
            $data=$helpers->jsonConverter([
                'status'=>'error',
                'users'=>'Authrorization not valid', 
            ]);
            
        }
        
        return $data;
    }

}
