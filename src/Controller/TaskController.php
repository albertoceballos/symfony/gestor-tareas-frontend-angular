<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints as Assert;
//servicios
use App\Service\Helpers;
use App\Service\JwtAuth;
//entidades:
use App\Entity\Users;
use App\Entity\Tasks;

class TaskController extends Controller {

    public function newTask(Helpers $helpers, JwtAuth $JwtAuth, Request $request, $id = null) {

        //recojo el token de POST
        $token = $request->get('authorization', null);

        //compruebo el token en el servicio creado para ello
        $authcheck = $JwtAuth->checkToken($token);

        //si devuelve true el servicio que comprueba el token
        if ($authcheck) {

            //consigo la identidad del usuario
            $identity = $JwtAuth->checkToken($token, true);

            //consigo de POST los datos de la tarea a introducir
            $json = $request->get('json', null);

            if ($json != null) {
                //decodifico el json con los datos de la tarea a introducir
                $params = json_decode($json);

                //asigno variables con los datos a introducir:
                $createdAt = new \DateTime('now');
                $updatedAt = new \DateTime('now');
                $userId = ($identity->sub != null) ? $identity->sub : null;
                $title = ($params->title != null) ? $params->title : null;
                $description = ($params->description != null) ? $params->description : null;
                $status = ($params->status != null) ? $params->status : null;

                //si tengo los datos básicos para introducir la tarea
                if ($userId != null && $title != null) {

                    //consigo el EntityManager y saco el objeto del usuario que introduce la tarea
                    $em = $this->getDoctrine()->getManager();
                    $user_repo = $em->getRepository(Users::class);
                    $user = $user_repo->findOneBy(['id' => $userId]);

                    //si el Id es null es que se va a crear tarea nueva si no es una tarea a actualizar
                    if ($id == null) {
                        //creo objeto de tarea y le sagino los datos
                        $task = new Tasks();
                        $task->setUser($user);
                        $task->setCreatedAt($createdAt);
                        $task->setDescription($description);
                        $task->setStatus($status);
                        $task->setTitle($title);
                        $task->setUpdatedAt($updatedAt);

                        $em->persist($task);
                        $flush = $em->flush();

                        //si se guarda bien asigno el array a devolver y si no el array con error
                        if ($flush == null) {

                            $data = [
                                'status' => 'success',
                                'code' => 200,
                                'msg' => ($id == null) ? 'Nueva tarea creada' : 'Tarea actualizada',
                                'data' => $task,
                            ];
                        } else {
                            $data = [
                                'status' => 'error',
                                'code' => 400,
                                'msg' => 'No se ha podido crear la tarea',
                            ];
                        }
                    } else {
                        //caso de actualizar una tarea:

                        $task_repo = $em->getRepository(Tasks::class);
                        $task = $task_repo->findOneBy(['id' => $id]);
                        if ($task != null) {
                            if (isset($identity->sub) && $identity->sub == $task->getUser()->getId()) {
                                $task->setDescription($description);
                                $task->setStatus($status);
                                $task->setTitle($title);
                                $task->setUpdatedAt($updatedAt);

                                $em->persist($task);
                                $flush = $em->flush();

                                //si se guarda bien asigno el array a devolver y si no el array con error
                                if ($flush == null) {

                                    $data = [
                                        'status' => 'success',
                                        'code' => 200,
                                        'msg' => ($id == null) ? 'Nueva tarea creada' : 'Tarea actualizada',
                                        'data' => $task,
                                    ];
                                } else {
                                    $data = [
                                        'status' => 'error',
                                        'code' => 400,
                                        'msg' => 'No se ha podido crear la tarea',
                                    ];
                                }
                            } else {
                                $data = [
                                    'status' => 'error',
                                    'code' => 400,
                                    'msg' => 'No se ha podido actualizar la tarea, no eres el propietario',
                                ];
                            }
                        } else {
                            $data = [
                                'status' => 'error',
                                'code' => 400,
                                'msg' => 'No se ha podido actualizar la tarea, no existe el id de tarea',
                            ];
                        }
                    }
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'msg' => 'Validación fallida',
                    ];
                }
            }
        } else {
            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Autorización no válida',
            ];
        }


        return $helpers->jsonConverter($data);
    }

    public function taskList(Request $request, JwtAuth $JwtAuth, Helpers $helpers) {

        //recojo el token de POST
        $token = $request->get('authorization', null);

        //compruebo el token en el servicio creado para ello
        $authcheck = $JwtAuth->checkToken($token);

        if ($authcheck) {
            //consigo la identidad del usuario
            $identity = $JwtAuth->checkToken($token, true);
            $em = $this->getDoctrine()->getManager();

            $dql = "SELECT t FROM App\Entity\Tasks t WHERE t.user=$identity->sub  ORDER BY t.id DESC";
            $query = $em->createQuery($dql);

            $page = $request->query->getInt('page', 1);
            $paginator = $this->get('knp_paginator');
            $items_per_page = 5;
            $pagination = $paginator->paginate(
                    $query, $page, $items_per_page
            );

            $total_items_count = $pagination->getTotalItemCount();

            $data = [
                'status' => 'success',
                'code' => 200,
                'msg' => 'Ok',
                'total_items_count' => $total_items_count,
                'page' => $page,
                'items_per_page' => $items_per_page,
                'total_pages' => ceil($total_items_count / $items_per_page),
                'data' => $pagination,
            ];
        } else {
            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Autorización no válida',
            ];
        }

        return $helpers->jsonConverter($data);
    }

    public function detail(Request $request, JwtAuth $JwtAuth, Helpers $helpers, $id = null) {
        //recojo el token de POST
        $token = $request->get('authorization', null);

        //compruebo el token en el servicio creado para ello
        $authcheck = $JwtAuth->checkToken($token);

        if ($authcheck) {
            $identity = $JwtAuth->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();
            $task_repo = $em->getRepository(Tasks::class);
            $task = $task_repo->findOneBy(['id' => $id]);

            if ($task != null && is_object($task) && $identity->sub == $task->getUser()->getId()) {
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'data' => $task,
                ];
            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'msg' => 'NO existe la tarea',
                ];
            }
        } else {
            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Autorización no válida',
            ];
        }
        return $helpers->jsonConverter($data);
    }

    public function search(Request $request, Helpers $helper, JwtAuth $JwtAuth, $search = null) {

        $token = $request->get('authorization', null);

        $authcheck = $JwtAuth->checkToken($token);

        if ($authcheck) {
            $identity = $JwtAuth->checkToken($token, true);
            $em = $this->getDoctrine()->getManager();
            $task_repo = $em->getRepository(Tasks::class);

            $filter = $request->get('filter', null);

            //filto de tareas
            if (empty($filter)) {
                $filter = null;
            } elseif ($filter == 1) {
                $filter = 'nueva';
            } elseif ($filter == 2) {
                $filter = 'pendiente';
            } else {
                $filter = "acabada";
            }


            //orden
            $order = $request->get('order', null);
            if (empty($order) || $order == 2) {
                $order = 'DESC';
            } else {
                $order = 'ASC';
            }

            //busqueda

            if ($search == null) {

                $dql = "SELECT t FROM App\Entity\Tasks t WHERE t.user=$identity->sub";
            } else {

                $dql = "SELECT t FROM App\Entity\Tasks t WHERE t.user=$identity->sub AND (t.title LIKE :search OR t.description LIKE :search)";
            }

            //set filter

            if ($filter != null) {
                $dql .= " AND t.status=:filter";
            }

            //set order
            $dql .= " ORDER BY t.id $order";


            $query = $em->createQuery($dql);

            //set parameter search
            if ($search != null) {
                $query->setParameter('search', "%$search%");
            }

            //set parameter sfilter
            if ($filter != null) {
                $query->setParameter('filter', $filter);
            }

            $tasks = $query->getResult();

            $countTasks = count($tasks);

            if ($countTasks != 0) {
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'num' => $countTasks,
                    'data' => $tasks,
                ];
            } else {
                $data = [
                    'status' => 'error',
                    'code' => 500,
                    'msg' => 'No hay coincidencias en la búsqueda',
                ];
            }
        } else {
            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Autorización no válida',
            ];
        }

        return $helper->jsonConverter($data);
    }

    public function remove(Request $request, Helpers $helpers, JwtAuth $JwtAuth, $id = null) {

        $token = $request->get('authorization', null);

        $authCheck = $JwtAuth->checkToken($token);

        if ($authCheck) {

            $identity = $JwtAuth->checkToken($token, true);
            $em = $this->getDoctrine()->getManager();
            $tasks_repo = $em->getRepository(Tasks::class);
            $task = $tasks_repo->findOneBy(['id' => $id]);

            if ($task != null && is_object($task) && $identity->sub == $task->getUser()->getId()) {

                $em->remove($task);
                $flush = $em->flush();
                if ($flush == null) {
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'msg' => 'Tarea borrada',
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 500,
                        'msg' => 'Error al borrar la tarea',
                    ];
                }
            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'msg' => 'No existe la tarea',
                ];
            }
        } else {

            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Autorización no válida',
            ];
        }

        return $helpers->jsonConverter($data);
    }

}
