<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\JwtAuth;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use App\Service\Helpers;
use App\Entity\Users;

class UserController extends AbstractController {

    public function newUser(Request $request, Helpers $helpers) {

        $json = $request->get('json', null);
        $params = json_decode($json);


        $data = [
            'status' => 'error',
            'code' => 400,
            'msg' => 'Error, usuario no creado',
        ];

        if ($json != null) {

            $createdAt = new \DateTime('now');
            $role = 'ROLE_USER';

            $name = (isset($params->name)) ? $params->name : null;
            $surname = (isset($params->surname)) ? $params->surname : null;
            $email = (isset($params->email)) ? $params->email : null;
            $password = (isset($params->password)) ? $params->password : null;

            $emailConstraint = new Assert\Email();
            $emailConstraint->message = "Este email no es válido";

            $validator = Validation::createValidator();
            $validate_email = $validator->validate($email, $emailConstraint);

            if ($email != null && count($validate_email) == 0 && $name != null && $surname != null && $password != null) {

                $user = new Users();
                $user->setCreatedAt($createdAt);
                $user->setRole($role);
                $user->setName($name);
                $user->setSurname($surname);
                $user->setEmail($email);

                //cifrar contraseña
                $pwd = hash('sha256', $password);
                $user->setPassword($pwd);


                $em = $this->getDoctrine()->getManager();
                $user_repo = $em->getRepository(Users::class);
                $isset_user = $user_repo->findBy(['email' => $email]);

                if (count($isset_user) == 0) {
                    $em->persist($user);
                    $em->flush();

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'msg' => 'Nuevo usuario creado con éxito',
                        'user' => $user,
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'msg' => 'Usuario no creado, duplicado',
                    ];
                }
            }
        }

        return $helpers->jsonConverter($data);
    }

    public function editUser(Request $request, Helpers $helpers, JwtAuth $JwtAuth) {

        //recojo el authorization del post
        $token = $request->get('authorization', null);

        //compruebo que el token sea válido
        $authCheck = $JwtAuth->checkToken($token);


        //si el token es válido:
        if ($authCheck) {
            //recojo el json del post y lo decodifico para convertirlo en un objeto PHP
            $json = $request->get('json', null);
            $params = json_decode($json);

            //conseguir los datos del usuario a actualizar
            $identity = $JwtAuth->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();
            $user_repo = $em->getRepository(Users::class);

            //conseguir el objeto user a actualizar
            $user = $user_repo->findOneBy(['id' => $identity->sub]);

            //array de error por defecto
            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Error, usuario no creado',
            ];

            if ($json != null) {


                $role = 'ROLE_USER';

                $name = (isset($params->name)) ? $params->name : null;
                $surname = (isset($params->surname)) ? $params->surname : null;
                $email = (isset($params->email)) ? $params->email : null;
                $password = (isset($params->password)) ? $params->password : null;

                $emailConstraint = new Assert\Email();
                $emailConstraint->message = "Este email no es válido";

                $validator = Validation::createValidator();
                $validate_email = $validator->validate($email, $emailConstraint);

                if ($email != null && count($validate_email) == 0 && $name != null && $surname != null) {


                    $user->setRole($role);
                    $user->setName($name);
                    $user->setSurname($surname);
                    $user->setEmail($email);

                    //cifrar contraseña
                    if ($password != null) {
                        $pwd = hash('sha256', $password);
                        $user->setPassword($pwd);
                    }


                    $isset_user = $user_repo->findBy(['email' => $email]);

                    if (count($isset_user) == 0 || $identity->email == $email) {
                        $em->persist($user);
                        $em->flush();

                        $data = [
                            'status' => 'success',
                            'code' => 200,
                            'msg' => 'Usuario actualizado con éxito',
                            'user' => $user,
                        ];
                    } else {
                        $data = [
                            'status' => 'error',
                            'code' => 400,
                            'msg' => 'Usuario no actualizado, duplicado',
                        ];
                    }
                }
            }
        } else {
            $data = [
                'status' => 'error',
                'code' => 400,
                'msg' => 'Autorización no válida',
            ];
        }
        return $helpers->jsonConverter($data);
    }

}
