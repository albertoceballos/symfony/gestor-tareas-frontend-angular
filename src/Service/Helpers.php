<?php

namespace App\Service;

use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;

class Helpers {
    
    //función para convertir un array en un objeto JSON:
    public function jsonConverter($data){
        
        $normalizers=[new GetSetMethodNormalizer()];
        $encoders=['json'=>new JsonEncoder()];
        
        $serializer=new Serializer($normalizers,$encoders);
        $json=$serializer->serialize($data,'json');
        
        $response= new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type','application/json');
        
        return $response;
        
        
    }
    
}
