<?php

namespace App\Service;

use Firebase\JWT\JWT;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Users;

class JwtAuth {

    private $manager;
    private $key;

    public function __construct(RegistryInterface $registryInterface) {
        $this->manager = $registryInterface;
        $this->key = "unaclavesecreta2121";
    }

    public function signup($email, $password, $getHash = null) {

        $em = $this->manager->getManager();
        $user_repo = $em->getRepository(Users::class);

        $user = $user_repo->findOneBy(['email' => $email, 'password' => $password]);

        //si encuentra el usuario y la contraseña:
        if (is_object($user)) {

            //Generar token
            $token = [
                "sub" => $user->getId(),
                "email" => $user->getEmail(),
                "name" => $user->getName(),
                "surname" => $user->getSurname(),
                "iat" => time(),
                //Expira en una semana:
                "exp" => time() + (7 * 24 * 60 * 60),
            ];

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);


            if ($getHash == null) {
                $data = $jwt;
            } else {
                $data = $token;
            }
        } else {
            $data = [
                'result' => 'Error',
                'data' => 'Usuario no encontrado',
            ];
        }

        return $data;
    }
    
    public function checkToken($jwt,$getIdentity=false){
        
        $auth=false;
        
        try {
            $decoded= JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $e) {
            $auth=false;
        } catch (\DomainException $e){
            $auth=false;
        }
        
        if(isset($decoded) && is_object($decoded) && isset($decoded->sub)){
            $auth=true;   
        }else{
            $auth=false;
        }
        
        
        if($getIdentity==false){
            return $auth;
        }else{
            return $decoded;
            
        }
        
        
    }

}
